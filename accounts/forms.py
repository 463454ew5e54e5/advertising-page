from django import forms
from ads.models import Advertisement


class AdvertisementEditForm(forms.ModelForm):
    action_value = forms.DecimalField(help_text="Please enter action value.", min_value=0)

    class Meta:
        model = Advertisement
        fields = ('title', 'action_value', 'page_url', 'img', 'status')
