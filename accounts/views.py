from django.contrib import messages
from django.contrib.auth import logout, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.shortcuts import render, redirect


# Create your views here.
from accounts.forms import AdvertisementEditForm
from accounts.models import UserProfile
from ads.models import Advertisement, Transaction


def signup_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.save()
            user_profile = UserProfile.objects.create(user=new_user, balance=100)
            login(request, new_user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'accounts/accounts_signup.html', {'form': form})


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('accounts:profile')
    else:
        form = AuthenticationForm()
    return render(request, 'accounts/accounts_login.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('home')


@login_required(login_url="/accounts/login/")
def profile_view(request):
    user = request.user.id
    ads = Advertisement.objects.all().filter(owner=user)
    user_profile = UserProfile.objects.get(user=user)
    transaction = Transaction.objects.filter(user_id=user)
    return render(request, 'accounts/profile.html', {'ads': ads, 'user_profile': user_profile,
                                                     'transaction': transaction})


@login_required(login_url="/accounts/login/")
def advertisement_detail(request, id):
    adv = Advertisement.objects.get(pk=id)
    return render(request, 'accounts/advertisement_detail.html', {'adv': adv})


@login_required(login_url="/accounts/login/")
def adv_edit(request, id):
    instance = Advertisement.objects.get(pk=id)
    if request.method == 'POST':
        edit_form = AdvertisementEditForm(instance=instance, data=request.POST, files=request.FILES)

        if edit_form.is_valid():
            edit_form.save()
            messages.success(request, 'Uaktualnienie reklamy ' \
                                      'zakończyło się sukcesem.')
        else:
            messages.error(request, 'Wystąpił błąd podczas uaktualniania reklamy.')
    else:
        edit_form = AdvertisementEditForm(instance=instance)

    return render(request, 'accounts/adv_edit.html', {'edit_form': edit_form})
