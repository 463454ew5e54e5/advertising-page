from django.urls import path, include
from . import views

app_name = 'accounts'
urlpatterns = [
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('signup/', views.signup_view, name='signup'),
    path('profile/', views.profile_view, name='profile'),
    path('profile/advertisement_detail/<int:id>/', views.advertisement_detail, name='advertisement_detail'),
    path('profile/advertisement_detail/adv_edit/<int:id>', views.adv_edit, name='adv_edit'),

]
