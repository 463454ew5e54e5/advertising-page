from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path
from django.contrib import admin
from django.views.generic import TemplateView

from ad_page import settings

urlpatterns = [
    path('', TemplateView.as_view(template_name="index.html"), name="home"),
    path('admin/', admin.site.urls),
    path('ads/', include('ads.urls')),
    path('accounts/', include('accounts.urls')),
]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_DIR)
