# Generated by Django 2.0.2 on 2018-02-12 15:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Advertisement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('action_value', models.DecimalField(decimal_places=4, max_digits=10)),
                ('page_url', models.URLField()),
                ('img', models.ImageField(blank=True, default='default.png', upload_to='')),
                ('status', models.CharField(choices=[('inactive', 'Nieaktywny'), ('active', 'Aktywny')], default='inactive', max_length=10)),
                ('owner', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='Advertisement', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url_link', models.URLField()),
                ('numbers_of_click', models.IntegerField()),
                ('advertisement_id', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='adver', to='ads.Advertisement')),
                ('user_id', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='transaction', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
