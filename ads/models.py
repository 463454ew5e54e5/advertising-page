from django.db import models
from django.contrib.auth.models import User
from accounts.models import UserProfile

STATUS_CHOICES = (
    ('inactive', 'Nieaktywny'),
    ('active', 'Aktywny'),
)


class Advertisement(models.Model):
    title = models.CharField(max_length=200)
    action_value = models.DecimalField(max_digits=10, decimal_places=4)
    page_url = models.URLField()
    img = models.ImageField(default='default.png', blank=True, )
    owner = models.ForeignKey(User, on_delete=models.CASCADE, default=None, related_name='Advertisement')
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='inactive')

    def __str__(self):
        return self.title


class Transaction(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, default=None, related_name='transaction')
    advertisement_id = models.ForeignKey(Advertisement, on_delete=models.CASCADE, default=None, related_name='adver')
    url_link = models.URLField()
    numbers_of_click = models.IntegerField()
