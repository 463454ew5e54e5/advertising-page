from django import forms
from ads.models import Advertisement


class AdvertisementForm(forms.ModelForm):
    title = forms.CharField(max_length=200, help_text="Please enter the title.")
    action_value = forms.DecimalField(help_text="Please enter action value.", min_value=0)
    page_url = forms.URLField(help_text="Please enter your page url.")
    img = forms.ImageField(help_text="Please enter image of your ad.")

    class Meta:
        model = Advertisement
        fields = ['title', 'action_value', 'page_url', 'img']
