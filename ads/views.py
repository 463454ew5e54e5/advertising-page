from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from accounts.models import UserProfile
from ads.forms import AdvertisementForm
from ads.models import Advertisement, Transaction


def ads_list_view(request):
    ads = Advertisement.objects.all()
    return render(request, 'ads/ads_list.html', {'ads': ads})


@login_required(login_url="/accounts/login/")
def add_advertisement(request):
    if request.method == 'POST':
        form = AdvertisementForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.owner = request.user
            instance.status = 'active'
            instance.save()
            return redirect('ads:list')
    else:
        form = AdvertisementForm()
    return render(request, 'ads/add_advertisement.html', {'form': form})


def ads_detail_view(request, id):
    adv = Advertisement.objects.get(pk=id)
    return render(request, 'ads/ads_detail.html', {'adv': adv})


@login_required(login_url="/accounts/login/")
def create_transaction_view(request, pk):
    user_id = request.user.id
    exist = Transaction.objects.filter(user_id=user_id, advertisement_id=pk).count()
    if not exist:
        url = "http://konandestylator.pythonanywhere.com/ads/reflink/{}-{}".format(user_id, pk)
        adv = Advertisement.objects.get(id=pk)
        transaction = Transaction()
        transaction.user_id = request.user
        transaction.url_link = url
        transaction.advertisement_id = adv
        transaction.numbers_of_click = 0
        transaction.save()
        return redirect('accounts:profile')
    else:
        return redirect('ads:list')


def redirect_view(request, slg):
    user_id, adv_id = slg.split("-")
    transaction = Transaction.objects.get(user_id=user_id, advertisement_id=adv_id)
    transaction.numbers_of_click = 1 + transaction.numbers_of_click
    url = transaction.advertisement_id.page_url
    value = transaction.advertisement_id.action_value
    owner_id = transaction.advertisement_id.owner
    owner = UserProfile.objects.get(user_id=owner_id)
    if owner.balance - value < 0:
        transaction.save()
        return redirect('home')
    else:
        owner.balance = owner.balance - value
        owner.save()
        user = UserProfile.objects.get(user_id=user_id)
        user.balance = user.balance + value
        user.save()
        transaction.save()
        return redirect(url)
