from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views

app_name = 'ads'

urlpatterns = [
                  path('', views.ads_list_view, name='list'),
                  path('add_advertisement/', views.add_advertisement, name='add_advertisement'),
                  path('<int:id>/', views.ads_detail_view, name='detail'),
                  path('transaction/<int:pk>/', views.create_transaction_view, name='create_transaction'),
                  path('reflink/<slug:slg>/', views.redirect_view, name='redirect'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
